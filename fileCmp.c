/*
Programming exercise: File Comparison

Write a C program to compare 2 binary files. It should take the 2 file names as command line parameters. 
(It has to be a command line parameter and **not** accepting input from the user after the program is launched.)

Notes:
Write the code to be as professional as possible. Handling all error cases.
Don’t use threading.
Pay attention to optimize reading from file and comparing
Don’t use static or dynamic memory > 8K for buffers.
Measure the amount of time it would take to compare two 1 MB files(which have some differences towards the end of the file). 
The time should be measured within the program. For modern processors, it shouldn't take more than 5 msec. 
The program should print the offset at which the first difference is found and first 16 bytes (in hex) from the files starting at the point they start differing.
*/

/*
 * Author: Santhosh, santhosh@colorado.edu
 * Date  : February 11, 2022
 */

// Headers
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define BUFFER_SIZE 1024

// Functions prints the offset and 16 bytes od data starting from the offset, where the difffernce is found
void print_result(void* file1, void* file2, int offset)
{
	FILE *f1, *f2;
	f1 = (FILE *)file1;
	f2 = (FILE *)file2;

	int nBytes = 16;

	printf("----------File comparison result----------\n\n");  

	// prints file offset, where the diffece is found
	printf("Difference found in file offest: %d\n\n",offset);

	// Prints 16bytes starting from the offset, where the difference in found         	
	fseek(f1, offset, SEEK_SET); 
	fseek(f2, offset, SEEK_SET);
	uint16_t *buffer1 = malloc(sizeof(char) * (nBytes/2));
	uint16_t *buffer2 = malloc(sizeof(char) * (nBytes/2));
	fread(buffer1, sizeof(uint16_t),nBytes/2,f1);
	fread(buffer2, sizeof(uint16_t),nBytes/2,f2);

	printf("File1: ");
	for(unsigned int j = 0; j < 8; j++)
		printf("%04x ", (buffer1[j] & 0xFF) << 8 | (buffer1[j] & 0xFF00) >> 8  );
	
	printf("\n");
	printf("File2: ");
	for(unsigned int j = 0; j < 8; j++)
		printf("%04x ", (buffer2[j] & 0xFF) << 8 | (buffer2[j] & 0xFF00) >> 8  );
	printf("\n");
}


/* @function: filesCmp()
 * @brief: compares two binary files
 * 
 * Parameters: file pointers to file1 and file2 and buffer size
 *
 * Returns: 0  <- if files are identical
 *			1  <- if files are different
 *          -1 <- if there is an error
 */
int filesCmp(char* file1, char* file2, int bSize)
{
	// can use file descrptors instead
    FILE *f1, *f2;
    f1 = fopen(file1, "rb");
    f2 = fopen(file2, "rb");

    // check for file pointer errors
    if (f1 == NULL || f2 == NULL)
        return -1;

    // Buffer dynamic allocation
    char* Buffer1 = malloc(sizeof(char)*bSize);

    // check for malloc errors
    if (Buffer1 == NULL) 
    {
        fclose(f1);
        fclose(f2);
        return -1;
    }
    char* Buffer2 = malloc(sizeof(char)*bSize);
    if (Buffer2 == NULL) 
    {
        free(Buffer1);
        fclose(f1);
        fclose(f2);
        return -1;
    }
    

    int bCount = 0; // to keep track on file offset

    // read the file into buffers and compare
    while(1)
    {
        unsigned int f1Read = fread(Buffer1, sizeof(char), bSize, f1);
        unsigned int f2Read = fread(Buffer2, sizeof(char), bSize, f2);

        if (f1Read != f2Read)
            goto FilesAreDifferent;

        for(unsigned int i = 0; i < f1Read; i++)
            if (Buffer1[i] != Buffer2[i]){
            	
            	//Print unmatch report
            	print_result( f1, f2, ((bCount * BUFFER_SIZE) + i));           

            	goto FilesAreDifferent;
            }
                

        if ((feof(f1) != 0) && (feof(f2) != 0))
            break; // both files have nothing more to read and are identical.

        goto Skip;

        FilesAreDifferent:
            free(Buffer1);
            free(Buffer2);
            fclose(f1);
            fclose(f2);
            return 1;

        Skip:
        	bCount++;
    }

    // Close files and free buffers
    free(Buffer1);
    free(Buffer2);
    fclose(f1);
    fclose(f2);
    return 0;
}

// main function
int main(int argc, char** argv)
{
	
	// Check for command line arguments
    if (argc != 3)
    {
        printf("ERROR: usage ./fcomp <binary_file_1> <binary_file_2>");
        return 0;
    }

    // clock initialization to measure time taken for the file comparison
    clock_t tic = clock();

    // call to file comparison function
    int result = filesCmp(argv[1], argv[2], BUFFER_SIZE);

    printf("-------------------------------------------\n");
    // time log and results display
    switch (result){
        case 0: 
        		printf("Files are identical.\n"); 
        		break;
        case 1: 
        		printf("Files are different.\n"); 
        		break;
        case -1:
        		printf("Error.\n"); break;
    }

    clock_t toc = clock();
    printf("\nTime elapsed: %f seconds\n", (double)(toc - tic) / CLOCKS_PER_SEC);

    return 0;
}